package rpi.sensehat.exception;

/**
 * Created by jcincera on 05/07/2017.
 */
public class InvalidSystemArchitectureException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = -4090365683972809296L;

    public InvalidSystemArchitectureException(String message) {
        super(message);
    }
}
