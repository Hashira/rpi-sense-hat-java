package rpi.sensehat.exception;

/**
 * Created by jcincera on 27/06/2017.
 */
public class CommunicationException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -5838222733553880483L;

    public CommunicationException(String message) {
        super(message);
    }

    public CommunicationException(Throwable cause) {
        super(cause);
    }

    public CommunicationException(String message, Throwable cause) {
        super(message, cause);
    }
}
